const axios = require('axios').default

const api = axios.create({
    baseURL: gateway,
    headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Cache-Control': 'no-cache',
        Pragma: 'no-cache',
        'X-Application-Name': 'app-name',
        'X-Application-Version': version
    }
})

api.interceptors.response.use(
    (response) => response.data,
    (error) => {
        throw error
    }
)

module.exports = { api }
