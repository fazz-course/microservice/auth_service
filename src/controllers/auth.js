const ctrl = {}
const model = require('../models')
const jwt = require('jsonwebtoken')
const bcr = require('bcrypt')
const hashing = require('../libs/hash')
const loggs = require('../libs/logger')(module)
const axios = require('axios')

const genToken = async (email, type = 'auth') => {
    try {
        const payload = {
            user: email
        }

        const KEYS = type != 'auth' ? process.env.JWT_KEYS_CONFIRM : process.env.JWT_KEYS

        const token = jwt.sign(payload, KEYS, { expiresIn: '1h' })

        return { token }
    } catch (error) {
        throw error
    }
}

ctrl.Login = async (req, res) => {
    try {
        const { email, password } = req.body
        const passDb = await model.GetByUser(email)

        console.log(passDb)

        if (passDb.length <= 0) {
            return res.status(401).send({ message: 'username tidak terdaftar' })
        }

        if (passDb[0].status <= 0) {
            return res.status(401).send({ message: 'silahkan aktivasi akun anda' })
        }

        const check = await bcr.compare(password, passDb[0].password)
        if (!check) {
            return res.status(401).send({ message: 'password salah' })
        }

        const result = await genToken(email)
        return res.status(200).send(result)
    } catch (error) {
        loggs.error(error)
        return res.status(401).send({ message: error })
    }
}

ctrl.AddData = async (req, res) => {
    try {
        const { email, password } = req.body
        const hashPassword = await hashing.securePassword(password)
        const { token } = await genToken(email, 'confirm')
        await model.Save({ email, password: hashPassword, confirm_token: token })
        await axios({
            url: `${process.env.EMAIL_SERVICE}?email=${email}&token=${token}`,
            method: 'POST'
        })
        return res.status(200).send({ message: 'silahkan check email anda' })
    } catch (error) {
        console.log(error)
        loggs.error(error)
        return res.status(500).send(error)
    }
}

ctrl.SetActiveUser = async (req, res) => {
    try {
        console.log(req.user)
        const data = await model.Activated(req.user)
        return res.status(200).send({ message: data })
    } catch (error) {
        loggs.error(error)
        return res.status(500).send(error)
    }
}

module.exports = ctrl
