const jwt = require('jsonwebtoken')

const auth = (req, res, next) => {
    const { token } = req.params

    if (!token) {
        return res.status(401).send({ message: 'silahkan login' })
    }

    jwt.verify(token, process.env.JWT_KEYS_CONFIRM, (err, decode) => {
        if (err) {
            return res.status(401).send({ message: err })
        } else {
            req.user = decode.user
            next()
        }
    })
}

module.exports = auth
