const express = require('express')
const routers = express.Router()
const ctrl = require('../controllers/auth')
const authVerif = require('../middleware/auth')

routers.post('/signin', ctrl.Login)
routers.post('/signup', ctrl.AddData)
routers.get('/verify/:token', authVerif, ctrl.SetActiveUser)

module.exports = routers
