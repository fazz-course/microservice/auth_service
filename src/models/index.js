const db = require('../config/db')
const model = {}

model.GetByUser = (email) => {
    return new Promise((reslve, reject) => {
        db.query('SELECT * FROM public.users WHERE email = $1', [email])
            .then((data) => {
                reslve(data.rows)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

model.GetTokenBy = (email) => {
    return new Promise((reslve, reject) => {
        db.query('SELECT token FROM public.users WHERE email = $1', [email])
            .then((data) => {
                reslve(data.rows)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

model.Activated = (email) => {
    return new Promise((reslve, reject) => {
        db.query(`UPDATE public.users SET status=1, confirm_token=null WHERE email = $1`, [email])
            .then((data) => {
                reslve('akun telah diverifikasi')
            })
            .catch((err) => {
                reject(err)
            })
    })
}

model.Save = ({ email, password, confirm_token }) => {
    return new Promise((reslve, reject) => {
        db.query(`INSERT INTO public.users (email, "password", status, confirm_token) VALUES($1, $2, 0, $3)`, [email, password, confirm_token])
            .then((data) => {
                reslve('data derhasil disimpan')
            })
            .catch((err) => {
                console.log(err)
                reject(err)
            })
    })
}

module.exports = model
